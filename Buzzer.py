#!/usr/bin/env python
# -*- coding: utf8 -*-

import RPi.GPIO as GPIO   #import the GPIO library
import time               #import the time library

class Buzzer(object):
    def __init__(self):
        GPIO.setmode(GPIO.BOARD)
        self.buzzer_pin1 = 29 #set to GPIO pin 5
        self.buzzer_pin2 = 31 #set to GPIO pin 5
        GPIO.setup(self.buzzer_pin1, GPIO.OUT)
        GPIO.setup(self.buzzer_pin2, GPIO.OUT)
        print("buzzer ready")

    def __del__(self):
        class_name = self.__class__.__name__
        print (class_name, "finished")

    def buzz(self,pitch, duration):   #create the function “buzz” and feed it the pitch and duration)
        if(pitch==0):
            time.sleep(duration)
            return
        period = 1.0 / pitch     #in physics, the period (sec/cyc) is the inverse of the frequency (cyc/sec)
        delay = period / 2     #calcuate the time for half of the wave  
        cycles = int(duration * pitch)   #the number of waves to produce is the duration times the frequency

        for i in range(cycles):    #start a loop from 0 to the variable “cycles” calculated above
            GPIO.output(self.buzzer_pin1, True)   #set pin 29 to HIGH
            GPIO.output(self.buzzer_pin2, False)   #set pin 31 to LOW
            time.sleep(delay)    #wait with pin 18 high
            GPIO.output(self.buzzer_pin1, False)    #set pin 29 to LOW
            GPIO.output(self.buzzer_pin2, True)   #set pin 31 to HIGH
            time.sleep(delay)    #wait with pin 18 low

    def play(self, tune):
        #GPIO.setmode(GPIO.BOARD)    
        #GPIO.setup(self.buzzer_pin1, GPIO.OUT)
        #GPIO.setup(self.buzzer_pin2, GPIO.OUT)
        x=0

        #Sucsess Sound
        print("Playing tune ",tune)
        if(tune==1):
            pitches=[690,970]
            duration=[0.15,0.2]
            for p in pitches:
                self.buzz(p, duration[x])  #feed the pitch and duration to the function, “buzz”
                time.sleep(duration[x] *0.5)
                x+=1

        #Error Sound
        elif(tune==2):
            
            pitches=[690,690]
            duration=[0.1,.2]
            for p in pitches:
                self.buzz(p, duration[x])  #feed the pitch and duration to the function, “buzz”
                time.sleep(duration[x] *0.5)
                x+=1
                
        #Startup Sound       
        if(tune==3):
            pitches=[690,0,690,0,690,0,970]
            duration=[0.1,0.07,0.1,0.04,0.1,0.09,0.2]
            for p in pitches:
                self.buzz(p, duration[x])  #feed the pitch and duration to the function, “buzz”
                time.sleep(duration[x] *0.5)
                x+=1
                
        elif(tune==4):
            pitches=[
                2637, 2637, 0, 2637,
                0, 2093, 2637, 0,
                3136, 0, 0,  0,
                1568, 0, 0, 0,
                2093, 0, 0, 1568,
                0, 0, 1319, 0,
                0, 1760, 0, 1976,
                0, 1865, 1760, 0,
                1568, 2637, 3136,
                3520, 0, 2794, 3136,
                0, 2637, 0, 2093,
                2349, 1976, 0, 0,
                2093, 0, 0, 1568,
                0, 0, 1319, 0,
                0, 1760, 0, 1976,
                0, 1865, 1760, 0,
                1568, 2637, 3136,
                3520, 0, 2794, 3136,
                0, 2637, 0, 2093,
                2349, 1976, 0, 0
            ]
            duration=[
                .12, .12, .12, .12,
                .12, .12, .12, .12,
                .12, .12, .12, .12,
                .12, .12, .12, .12,
                .12, .12, .12, .12,
                .12, .12, .12, .12,
                .12, .12, .12, .12,
                .12, .12, .12, .12,
                .09, .09, .09,
                .12, .12, .12, .12,
                .12, .12, .12, .12,
                .12, .12, .12, .12,
                .12, .12, .12, .12,
                .12, .12, .12, .12,
                .12, .12, .12, .12,
                .12, .12, .12, .12,
                .09, .09, .09,
                .12, .12, .12, .12,
                .12, .12, .12, .12,
                .12, .12, .12, .12,
            ]
            for p in pitches:
                self.buzz(p, duration[x])  #feed the pitch and duration to the function, “buzz”
                time.sleep(duration[x] *0.5)
                x+=1

